<?php
namespace frontend\models;

use common\models\UserProvider;
use yii\base\Model;
use common\models\User;
use yii;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $first_name;
    public $last_name;
    public $password;
    public $facebookId;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['first_name','last_name'],'string','max'=>32],
            ['facebookId','integer'],
//            ['email', 'trim'],
//            ['email', 'required'],
//            ['email', 'email'],
//            ['email', 'string', 'max' => 255],
//            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        $transaction=Yii::$app->db->beginTransaction();
        if (!$user->save()){
            $transaction->rollBack();
            return null;
        }
        if ($this->facebookId) {
            if (!UserProvider::newModel($user->id,UserProvider::FACEBOOK,$this->facebookId)){
                $transaction->rollBack();
                return null;
            }
        }
        $transaction->commit();
        return $user;
    }
}

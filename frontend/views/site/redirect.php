
<head>
    <script>
        var result=<?= json_encode(Yii::$app->session->get('clientAuth'))?>;

        function popupWindowRedirect(url, enforceRedirect,result)
        {
            console.log(result);
            if (window.opener && !window.opener.closed) {
                if (enforceRedirect === undefined || enforceRedirect) {
                    window.opener.location = url;
                }
                window.opener.focus();
                window.close();
                window.opener.clientAuth(result);

            } else {
                window.location = url;
            }
        }
        popupWindowRedirect("\/", false,result);
    </script>
</head>
<h2 id="title" style="display:none;">Redirecting back to the &quot;My Application&quot;...</h2>
<h3 id="link"><a href="/">Click here to return to the &quot;My Application&quot;.</a></h3>
<script type="text/javascript">
    document.getElementById('title').style.display = '';
    document.getElementById('link').style.display = 'none';
</script>

import {Injectable} from '@angular/core';
import { environment } from '../../environments/environment';
import {
  Http,
  XHRBackend,
  Headers,
  RequestOptions,
  Request,
  Response,
  RequestOptionsArgs
} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class BaseHttp extends Http {

  constructor(backend: XHRBackend, options: RequestOptions) {
    super(backend, options);
  }
  public baseUrl = environment.apiUrl;

  request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    let token = localStorage.getItem('auth_token');
    if (typeof url === 'string') {
      if (!options) {
        options = {headers: new Headers()};
      }
      if (!options.headers){
        options.headers= new Headers();
      }
      options.headers.set('Authorization', `Bearer ${token}`);
      // options.headers.append('Content-Type', `application/x-www-form-urlencoded`);
      options.headers.set('Content-Type', `application/json`);

      url = this.baseUrl + url;
    } else {
      url.headers.set('Authorization', `Bearer ${token}`);
      // url.headers.append('Content-Type', `application/x-www-form-urlencoded`);
      url.headers.set('Content-Type', `application/json`);
      url.url = this.baseUrl + url.url;
    }
    return super.request(url, options).catch(this.catchAuthError(this));
  }

  private catchAuthError(self: BaseHttp) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      // console.log(res);
      if (res.status === 401 || res.status === 403) {
        // if not authenticated
        // console.log(res);
      }
      return Observable.throw(res);
    };
  }

}

/**
 * Created by chungtn on 13/02/2017.
 */
import {Component, OnInit, NgZone} from '@angular/core';
import {AuthenticationService,WindowRef} from '../../_services/index';
import {Router, ActivatedRoute} from '@angular/router';
import { environment } from '../../../environments/environment';
import {Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';



@Component({
    selector: 'auth-login',
    moduleId: module.id,
    templateUrl: 'auth.html',
})

export class AuthComponent implements OnInit {
    FBlogin:boolean;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private windowRef: WindowRef,
        ) {
        var self=this;
        windowRef.nativeWindow.clientAuth =function (result) {
            if (result && result.action){
                if (result.action=='register') {
                    localStorage.setItem('userProvider',JSON.stringify(result.user));
                    return self.router.navigate(['/register']);
                }
                if (result.action=='login'){
                    localStorage.setItem('currentUser', JSON.stringify(result.user));
                    localStorage.setItem('auth_token', result.user.access_token);
                    return self.router.navigate(['/home']);
                }
                if (result.action=='bind'){
                    localStorage.setItem('userProvider',JSON.stringify(result.user));
                }
            }
        }
    }
    public baseUrl = environment.apiUrl;

    onLoginClick(type) {
        localStorage.setItem('clientType',type);
        this.windowRef.nativeWindow.popupCenter(this.baseUrl+'/api/user-provider/auth?authclient='+type, '_blank',860,480);
    }
    ngOnInit() {
        this.FBlogin=localStorage.getItem('facebookUser')?true:false;


    }
}
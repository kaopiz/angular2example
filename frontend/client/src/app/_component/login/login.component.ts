﻿import {BaseComponent} from "../base.component";
import { Component, OnInit } from '@angular/core';
import { AlertService, AuthenticationService } from '../../_services/index';
import {Injector} from "@angular/core";

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent extends BaseComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        injector:Injector) {
        super(injector);
    }

    ngOnInit() {
        // reset login status
        this.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    }
    logout(){
        if (localStorage.getItem('currentUser')) {
            this.authenticationService.logout().subscribe(
                data => {
                    localStorage.removeItem('currentUser');
                    localStorage.removeItem('auth_token');
                    localStorage.removeItem('userProvider');
                }
            );
        }
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}

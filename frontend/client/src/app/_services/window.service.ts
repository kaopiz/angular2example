import { Injectable } from '@angular/core';

function _window() : any {
    // return the global native browser window object
    return window;
}

@Injectable()
export class WindowRef {
    get nativeWindow() : any {
        return _window();
    }
    constructor(
        // private nativeWindow: nativeWindow,
    ) {
        this.nativeWindow.popupCenter =function(url, target,w,h) {
                // var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                // var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var left = ((width / 2) - (w / 2)) ;
                var top = ((height / 2) - (h / 2)) ;
                var newWindow = window.open(url, target, 'scrollbars=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                // Puts focus on the newWindow
                if (window.focus) {
                    newWindow.focus();
                }
            }
    }

}
﻿import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { Response } from '@angular/http';
import { ApiRequestOptions } from '../_helpers/api';
import { BaseHttp } from '../base-services/base-http';

@Injectable()
export class AuthenticationService extends ApiRequestOptions{
  constructor(protected http:BaseHttp) {
    super();
  };

    login(username: string, password: string) {
      return this.http.post('/api/user/login', { username: username, password: password })
      // return this.http.post('/api/user/login', JSON.stringify({ username: username, password: password }))
            .map((response: Response) => {
                let user = response.json();
                if (user) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    localStorage.setItem('auth_token', user.access_token);
                }
            });
    }

    logout() {
        return this.http.get('/api/user/logout')
            .map((response: Response) => {
               if (response) {
                   localStorage.removeItem('currentUser');
                   localStorage.removeItem('auth_token');
               }
            });

    }
    clientLogin(client) {
        return this.http.get('/api/user-provider/client-login')
    }
    clientUpdate() {
        return this.http.get('/api/user-provider/client-login')
    }
}

<?php

namespace app\module\api;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\module\api\controllers';

    public $option_actions = ['login','register','forgot-password','forgot-username','prefecture-list','options'];
    public $option_controllers = ['user-provider'];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if (!in_array($this->module->requestedAction->id, $this->option_actions)&&!in_array(Yii::$app->controller->id,$this->option_controllers)) {
            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBasicAuth::className(),
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ];
        }
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
//                'application/xml' => Response::FORMAT_XML,
            ]
        ];

        $behaviors['corsFilter']= [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                //#TODO change to 127.0.0.1 at production
                // restrict access to
                'Origin' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
            ]
        ];

        return $behaviors;
    }
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->loginUrl = null;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // custom initialization code goes here
    }

}

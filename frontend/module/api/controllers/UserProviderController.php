<?php
namespace app\module\api\controllers;

use common\models\UserProvider;
use yii;
use yii\authclient\AuthAction;

class UserProviderController extends RestBaseController
{

    public function actions()
    {
        return [
            'auth' => [
                'class' => 'common\models\AuthAction',
                'successCallback' => [$this, 'successCallback'],
                'redirectView' => '//site/redirect',
            ],
        ];
    }


    public function successCallback($client)
    {
        /** @var yii\authclient\ClientInterface $client */
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        $result= UserProvider::clientAuth($client);
        Yii::$app->session->set('clientAuth',$result);
    }
    public function actionClientLogin()
    {
        /** @var yii\authclient\ClientInterface $client */
        $client=Yii::$app->session->get('client');
        $result= UserProvider::clientAuth($client);
        return $result;

    }

    public function findModel($condition = null)
    {
        /** @var $model UserProvider */
        $model = UserProvider::findActive($condition)->one();
        return $model;
    }

}



